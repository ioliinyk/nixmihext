package NIXMIHEXT

public object #'WebReports DownloadED'# inherits WEBREPORTS::WebReportsExportDestinations::#'WebReports DesktopED'#

	override	String	fDestinationType = 'download'
	override	List	fMandatoryParameters = { { 'export_location', { 'browser', 'desktop', 'livelink', 'email', 'server', 'form', 'workflow', 'request', 'ftp' } }, { 'mimetype', {  } } }
	override	List	fOptionalParameters


	
	override function Void _runDestination( Assoc response, DAPINODE WRnode )
	
			Integer		fileLen				
			String		exportData			
			String		inlineOrAttach		
	
			// ** Desktop Export Destination **
			String		contentHeader		 = ""
			String		contentType			 = .fRequest.mimetype
			String		fileName			 = !.fRequest.desktopDownloadName ? WRnode.pName : .fRequest.desktopDownloadName
			List		result				 = Pattern.Find( fileName[ -5 : -1 ], ".[A-Za-z]+" )	// determine if we have an extension on the file name
			Assoc		extDefs				 = WRnode.pExtendedData.exportDefinitions
			String		tempExt				 = ""
	
	
			if ( !isDefined( extDefs ) )
	
				extDefs = Assoc.CreateAssoc()
	
			end
	
			if ( extDefs.mimetype != "text/html" && .fRequest.export_location != "desktop" )
	
				inlineOrAttach = "inline"
	
			else
	
				inlineOrAttach = "attachment"
	
			end
	
			// pdf isn't supported for this type (spontaneous) of export so we return an error
	
			if ( .fRequest.mimetype == "application/pdf" )
	
				response.error.message = [WEBREPORTS_LABEL.MimeTypeNotSupportedForExportToDesktop]
				response.error.detail = .fRequest.mimetype
	
				return
	
			end
	
			// perform tag replacement on the file name
			fileName = .fInstance._processTags( fileName, .fRequest, response )
	
			// now make sure there are no invalid characters left
			fileName = $WebDoc.WebDocUtils.Space2UnderScore( fileName )
	
			// if there is no extension we will add one
	
			if ( !isDefined( result ) )
	
				if ( .fRequest.mimetype == "text/csv" )
	
					fileName += ".csv"
	
				else
	
					// Only append the extension if there's actually an extension registered, 
					// otherwise the whole fileName is wiped out:
					tempExt = $WebDsp.mimetypepkg.getmimetypefileext( .fRequest.mimetype )
	
					if ( IsDefined( tempExt ) && IsNotError( tempExt ) )
	
						fileName += "." + tempExt
	
					end
	
				end
	
			end
	
			if ( IsDefined( response.data.exportLocation ) )
	
				response.data.exportLocation.desktopDownloadName = fileName
	
			end
	
			// build the output string
			exportData = ._generateOutputString( response )
	
			// replace relative path with absolute so that the images render for the apps that can handle it
			exportData = ._updateSRCElements( exportData )
	
			// We don't want to write to socket if this is an API Call as the output is delivered to the API and not the browser.
	
			if ( !( .fAPICall ) )
	
				// only calculate the length once we've finished modifying the data	
				fileLen = Length( Bytes.NewFromString( exportData ) )
	
				// output the headers
				contentHeader += Str.Format( "Content-Type: %1;%2%3", contentType, 'charset=UTF-8', Web.CRLF )
				contentHeader += Str.Format( "Content-Length: %1%2", fileLen, Web.CRLF )
				contentHeader += Str.Format( "Set-Cookie: %1%2", 'fileDownload=true; path=/', Web.CRLF )
	
				if ( ( ( $WebLL.WebUtils.NetscapeClient( .fRequest ) ) ||
					    ( $WebLL.WebUtils.ChromeClient( .fRequest ) ) ) && 
					 !( $WebLL.WebUtils.MacClient( .fRequest ) ) )
	
					contentHeader += 
						Str.Format( "Content-Disposition: %1; filename*=utf-8''%2", inlineOrAttach, fileName ) + Web.CRLF
	
				else
	
					contentHeader += Str.Format(
									 	"Content-Disposition: %1; filename=%2",
									 	inlineOrAttach,
									 	$WebDoc.WebDocUtils.GetDownloadFileName( fileName ) ) + 
									 Web.CRLF
	
				end
	
				contentHeader += Web.CRLF
	
				// output the content
				Web.Write( .fRequest.ctxOut, contentHeader )
				Web.Write( .fRequest.ctxOut, exportData )
	
			end
	
			// set the audit data
			._setAuditInfo()
	
		end

	
	override function Void _runDestinationPre( Assoc response, DAPINODE WRnode )
	
			Assoc		extDefs		 = WRnode.pExtendedData.exportDefinitions
	
	
			///////////////////////////////////////////////////////////////////////////////////////
			//	Copy default behavior into the request only if an explicit export is not present //
			///////////////////////////////////////////////////////////////////////////////////////	
	
			if ( IsDefined( extDefs ) )
				extDefs.export_location = .fRequest.export_location
				if ( IsDefined( extDefs.export_location ) &&
					   ( extDefs.export_location != "" ) &&
					   !IsFeature( .fRequest, "exportOrig" ) )
	
					// We have default export behavior specified in extended data
					response.data.exportLocation = extDefs
	
					._mergeDestinationDefaults( extDefs )
	
				end
	
			end
	
			////////////////////////////////////////////////////////////////////////////////////////
			// At this point the request shouold contain everything we need to process the export //
			////////////////////////////////////////////////////////////////////////////////////////
	
			._validateExportParms( response )
	
		end

	
	override function _setParameters()
	
			// set the list of mandatory parameters and their respective valid types in a list of name-value pairs.
			// if a parameter is mandatory but can be of any value, use an empty list for the second argument
			.fMandatoryParameters = { { "export_location", .fDestinationTypes }, { "mimetype", {} } }
	
			// we use this feature to track parameters that are not mandatory but must have one of a specific list of
			// values when present
			
			.fDestinationRoot.fDestinationTypes = List.SetAdd( .fDestinationRoot.fDestinationTypes, 'download' )
			
			.fOptionalParameters = {}
	
		end

	
	override function Void _validateExportParms( Assoc response )
	
			Boolean		throwError				
			Integer		i						
			String		errMsg					
	
			List		destinationMimeTypes	 = .fPrgCtx.IniGet(
													  'webreports',
													  'DestinationMimeTypes',
													  $WebReports.fDestinationMimeTypesDefaultValue )
	
	
			for i = 1 to Length( .fMandatoryParameters )
	
				// we don't need a nexturl if we're on a schedule
	
				if ( !( ( ( .fAPICall ) OR ( Response.data.cacheid ) ) AND
						  ( .fMandatoryParameters[ i ][ 1 ] == "nexturl" ) ) )
	
					// Switch any parms named 'myNodeName' in the request to be the 'nodeName' parm:
					// 		In some weblingo forms, the original input fields' name was 'nodeName', which 
					// 		results in an ambiguous DOM path of something like document.forms[0].nodeName.  
					// 		This causes issues in js/jQuery (loaded by the CS Header), as that path 
					//		could reference both a form input field or the nodeName property of the form.
					//		The weblingo in question (webreports-export.html) has been updated so the name
					//		of the field is 'myNodeName' (which we use in other destinations).
	
					if ( ( .fMandatoryParameters[ i ][ 1 ] == "nodeName" ) &&
						   ( IsFeature( .fRequest, "myNodeName" ) ) &&
						   ( IsFeature( .fRequest, "exportOrig" ) ) &&
						   ( .fRequest.exportOrig == "exportOrig" ) )
	
						// Convert 'myNodeName' in the request to 'nodeName':
						.fRequest = $LLIAPI.RecArrayPkg.AddFieldsToRecord( .fRequest, { "nodeName" } )
						.fRequest.nodeName = .fRequest.myNodeName
	
					end
	
					// Check mandatory parameters are present
	
					if ( !( IsFeature( .fRequest, .fMandatoryParameters[ i ][ 1 ] ) ) )
	
						response.error.message = [WEBREPORTS_ERROR.MandatoryParameterMissing]
						response.error.detail = .fMandatoryParameters[ i ][ 1 ]
	
						return
	
					end
	
					// Check mandatory parameters are valid 
	
					if ( !( .fRequest.( .fMandatoryParameters[ i ][ 1 ] ) IN .fMandatoryParameters[ i ][ 2 ] ) )
	
						throwError = FALSE
						
						/*
						if ( .fMandatoryParameters[ i ][ 1 ] == 'mimetype' )
	
							// Don't throw an error when adding a version using the external conversion agent
							// or if mimetype is in the list of approved destinationMimeTypes.
	
							if ( !( ( .fConversionComplete ) &&
								     ( .fDestinationType == 'livelink' ) &&
								     ( .fDestinationSubType == 'livelinkversion' ) ) && 
								 !( .fRequest.mimetype IN destinationMimeTypes ) )
	
								throwError = TRUE
								errMsg = Str.Catenate( destinationMimeTypes, "," )
								errMsg = errMsg[ 1 : -2 ]	// chop the last comma off
	
							end
	
						elseif ( Length( .fMandatoryParameters[ i ][ 2 ] ) != 0 )
	
							throwError = TRUE
							errMsg = Str.Catenate( .fMandatoryParameters[ i ][ 2 ], "," )
							errMsg = errMsg[ 1 : -2 ]	// chop the last comma off
	
						end
						*/
						
						if ( throwError )
	
							response.error.message = Str.Format( [WEBREPORTS_ERROR.MandatoryParameterMustBeOneOf1], errMsg )
							response.error.detail = .fRequest.( .fMandatoryParameters[ i ][ 1 ] )
	
							return
	
						end
	
					end
	
				end
	
			end	// for
	
			for i = 1 to Length( .fOptionalParameters )
	
				// Check optional parameters are valid
	
				if ( IsFeature( .fRequest, .fOptionalParameters[ i ][ 1 ] ) )
	
					if ( !( .fRequest.( .fOptionalParameters[ i ][ 1 ] ) IN .fOptionalParameters[ i ][ 2 ] ) &&
						   ( Length( .fOptionalParameters[ i ][ 2 ] ) != 0 ) )
	
						response.error.message = Str.Format(
													 [WEBREPORTS_LABEL.OptionalParameterMustBeOneOf1],
													 Str.Catenate( .fOptionalParameters[ i ][ 2 ], "," ) )
						response.error.message = response.error.message[ 1 : -2 ]	// chop the last comma off
						response.error.detail = .fRequest.( .fOptionalParameters[ i ][ 1 ] )
	
						return
	
					end
	
				end
	
			end
	
		end

end
