package NIXMIHEXT::WebNodeActions

public object DownloadReport inherits NIXMIHEXT::WebNodeActions::#'WebReport WebnodeActions'#

	override	Boolean	fEnabled = TRUE


	
	override function List _Nodetypes()
	
			return { $TypeWebReports }
	
		end

	/*
			* This method returns the prototype describing the parameters expected in the request
			*
			* See .CheckArgsUtil() in any request handler for documentation detailing the format of this list
			*
			* @return {List} 
			*/
	override function List _Prototype()
	
			return {
				{'export_location', StringType,'Export Location',FALSE,'download'} \
			}
	
		end

	
	override function Assoc _SubclassExecute( Object webNode, Record request )
	
			Assoc		response	 = ._NewResponse()
				
				Assoc		cacheResult
				Assoc		timings
				Boolean		APIcall
				Frame		frame1, frame2, frame3
				Object		BuildInstance, DSinstance, EDinstance
				Assoc		rtnVal
				Assoc		runAs
				Dynamic		perfInfo
				Object		uSession
				Assoc		extData
				Assoc 		result
				String 		tempString
				Dynamic 	tmp
				Boolean 	securePromptingRequired
				String 		logFile
				
				Object		prgCtx = request.prgCtx
				DAPINODE	WRnode = request.node 
				Object		webreputils = $Webreports.WebreportsUtils
				Object		wrWebNode = $webnode.WebNodes.GetItem( $TypeWebReports )
			
				
				// We can change this as follows:
				
				// Create a build instance very early on and use the instance to store request/response data including the result set
				// Use the special script to return the correct data source pointer according to source type (this is not an instance). 
				// Then we just invoke the correct generic script for each data source, e.g. _executeQuery. 	
				// Could we use a frame for the build instance to ensure that it gets cleaned up properly? 
			
			    //	Script error handling is per standard Livelink response consisting of three string components in the response.error assoc. 
			    //	1) error.message - If this is defined an error message will be sent to the user containing this text.  If this is not set no
			    //	error will be displayed to the user irrespective of the other two.  Following calls to subroutines we should check if this 
			    //	is defined and return from RunReport if it is.
			    //	2) error.detail - If this is defined a button will display on the error page.  When clicked this value will be displayed.
			    //	3) error.prefix - If set this will be shown at the top of the error page.
			
				// Move this so it's set when we create a DS object. Add an insert timings method to DS object?
				// Alternatively we could do the timings stuff here? 
				// Guess it depends on exactly what we are timing. If we just want the time for the external system (if any)?
			
				timings.beginRunReport = Date.Tick()
				timings.beginQuery = 0
				timings.endQuery = 0
				timings.endRunReport = 0
				
				// Need to detect an APIcall but we need to avoid the possibilty that someone could fake an apicall
				// Thus we make sure that both request and response have APIcall set as a user can't set response.apicall. 
				// Right now we need request.APIcall too as it is used by some functions that can only access request. 
				APIcall = IsFeature( response, 'APIcall' ) ? response.APIcall : false
			
				if ( ( IsFeature( request, 'APIcall' ) ) and ( request.APICall ) )
					if !( response.APIcall )
						// If we have APIcall in the request but not the response then a user has deliberately put APICall in the request.
						// Not sure what they are up to but for now we will just quit relatively quietly and log a message
						echo( Str.Format( '************************ WebReports Security Issue, user %1 added APICall to the request.', prgCtx.USession().fUserID ) )
						response.error.message = [WEBREPORTS_ERROR.InvalidRequest]
						
						return
					end
				end
				
				uSession = prgCtx.USession()        
				
			    // Any instances being created should be assigned as framewrappers so that they can be automatically deleted
				response.error.prefix = 'WebReport'
	
				// We may need this set for other checks so it has been separated from the performerID block
				if ( ( IsFeature( request, 'node' ) ) && ( IsDefined( request.node ) ) && !( IsError( request.node ) ) )
				    extData = request.node.pExtendedData
				end
				
				////////////////////////////////////////////////////////////////////////////////////
				//  Check we are licensed														  //
				////////////////////////////////////////////////////////////////////////////////////
				if !( APIcall ) // API calls have already handled licensing checks
					result = $WebReports.WebReportsLicense.getLicenseStatus( prgCtx, extData )
			
					if ( result.restrict )
						// Return an error page to user
						response.error.message = result.message
						
						return
					end
				end
				
				/////////////////////////////////////////////////////////////
			    // Opportunity for "Run As" feature to change Program Context
			    /////////////////////////////////////////////////////////////	
				if ( IsDefined( extData ) )	// 2934 
				    if ( ( IsFeature( extData, 'performer_ID' ) ) && ( IsDefined( extData.performer_ID ) ) && ( extData.performer_ID != '' ) )
						perfInfo = UAPI.GetById( uSession.fSession, Str.StringToValue( extData.performer_ID ) )
			
						if !( IsError( perfInfo ) )
							runAs.User = perfInfo[ 1 ].Name
							runAs.Password = Undefined
							runAs.Domain = ''
							rtnval = $WebReports.WebReportsUtils.GetUserPrgCtx( runAs )
							
							if ( rtnval.ok != TRUE )
								response.error.prefix = [WEBREPORTS_LABEL.WebReportRunAsErrorPrefix]
								response.error.message = rtnval.error
								
				    			return
				    		else
				    			// put this into the response for the license
				    			response.runAs = prgCtx.USession().fUserId
				    			prgCtx = rtnval.prgCtx
				    		end
				    	else
				    		response.error.prefix = [WEBREPORTS_LABEL.WebReportRunAsErrorPrefix]
				    		response.error.message = Error.ErrorToString( perfInfo )
				    		
				    		return
				    	end
					end
				end
				
				// See if we have a cached request to use. Only new parameters will be added to the existing request
				tempString = webRepUtils.fReqCacheParm
				
				if ( IsFeature( request, tempString ) )
					rtnval = webreputils._mergeCacheParms( prgCtx, request,request.( tempString ) ) // Failure currently only means that the cache entry isn't there - probably expired
					
					if ( rtnval.ok != TRUE )
						response.error.message = [WEBREPORTS_ERROR.ThisPageHasExpiredAndNeedsToBeReEntered]
						
			    		return
			    	else
			    		request = rtnval.request
			    	end
				end
				
			    ////////////////////////////////////////////////////////////////////////////////////
			    //   Extract the view file                                                        //
			    ////////////////////////////////////////////////////////////////////////////////////
			    if ( IsError( WRnode ) )
			    	tmp =  WRnode
			    	response.error.message = Error.ErrorToString( tmp )
					
			    	return
				end
						
			    ////////////////////////////////////////////////////////////////////////////////////////////////////////
			    //	Create a datasource instance - returns the correct datasource for the specified sourcetype        //
			    ////////////////////////////////////////////////////////////////////////////////////////////////////////
				frame1 = $Webreports.WebReportsDataSources._initDataSourceObject( prgCtx, request, response, WRnode, APIcall )
				DSinstance = frame1.fTempObject	
			
			    ///////////////////////////////////////////////////////////////////////////////////////////////////////
				//	Instantiate temporary instance to execute build reports in.           							 //
				// 	This has been moved to here so we can check for a runif tag before anything else.                //
				///////////////////////////////////////////////////////////////////////////////////////////////////////
				frame2 = $RKTEngine.WebReportsProcessor.New( prgCtx, request, DSinstance, undefined )
				buildInstance = frame2.fTempObject
				
				if !( buildInstance )
					response.error.message = [WEBREPORTS_ERROR.UnableToCreateWebReportsInstance]
					
					return
				end	
				
			    ////////////////////////////////////////////////////////////////////////////////////////////////////////	
				// Prior to doing anything else, see if we have a RUNIF tag in the reportview and if so, check if 	  //	
				// we should run or not																				  //
			    ////////////////////////////////////////////////////////////////////////////////////////////////////////
			    if !( buildInstance._evaluateRunIf( request, response ) )
					// The evaluate script sets the response flag and any error messages
			    	return 
			    end
			
			    ////////////////////////////////////////////////////////////////////////////////////////////////////////
			    //	Manage any data prompting if required														      //
			    ////////////////////////////////////////////////////////////////////////////////////////////////////////
				DSinstance._DSsetupPrompts( request ) // Can we manage apicall through the request? 
			
				if ( response.error.message )
					return
				end
				
				// If prompting isn't already required, we check to see if we are using secureRequestTokens
				// and we were not called as an API (SWRs etc.), and then we call a function to see if a token was provided and if not
				// we force prompting
				
				// Check if we need to get more information via prompts	
				
				// If secure tokens are being used, we always call the requireSecureTokenPrompt first as it will fill the token into the response. This is always required.
				securePromptingRequired = false
				
				if ( ( $WebReports.WebReportsUtils._requireSecureTokenPrompt( prgCtx, request, response ) ) AND ( !( IsFeature( request, 'APIcall' ) && ( request.APIcall ) ) ) )
					securePromptingRequired = true
				end
				
				if ( ( DSinstance.fkeepPrompting ) OR ( securePromptingRequired ) )
					// Even though we are going to prompt anyway, invoke this function as it will put a token in the response that can be used later
					wrWebNode._performBrowserPromptActions( request, DSinstance )
					
					// Cache the original request parameters.  
					// This is to prevent having to pass-through ALL passed parameters in the request for the prompt screen:
					cacheResult = $Webreports.WebReportsUtils._cacheRequestParms( prgCtx, request )
				
					if ( cacheResult.ok == TRUE )
						if ( !IsFeature( request, tempString ) )
							request = $LLIAPI.RecArrayPkg.AddFieldsToRecord( request, { tempString } )
						end
						
						if ( !IsFeature( request, tempString ) )
							request = $LLIAPI.RecArrayPkg.AddFieldsToRecord( request, { tempString } )
						end
						
						request.( tempString ) = cacheResult.cacheid
						request.( tempString ) = cacheResult.cacheid
					end
					
					//return
				end
			
				//************** If further prompting is required execution ends here *****************
			
			    ////////////////////////////////////////////////////////////////////////////////////////////////////////
				//  Add any parameter defaults to the request 														  //
			    ////////////////////////////////////////////////////////////////////////////////////////////////////////
				request = DSinstance._addDefaultParmsToRequest( request)
			  	
				timings.beginQuery = Date.Tick()
			
			    if ( DSinstance.fHasQuery )
			    	DSinstance._runAllDSscripts( request, buildInstance )
			    	
					if ( response.error.message )
						return
					end
					
					request = DSinstance.fRequest
				else
					$WebReports.WebReportsUtils._RunInBackground( request, BuildInstance.fDSObject.fWRextData.exportDefinitions, BuildInstance.fDSObject.fAPIcall )
			    end	
			    
				timings.endQuery = Date.Tick()
				
				// check for the &WRBuildTime parameter to create a log for the timings:
				if ( ( IsFeature( request, 'WRBuildTime' ) ) && ( Str.Lower( request.WRBuildTime ) == 'true' ) )
					$RKTEngine.Timer.Reset()
					$RKTEngine.Timer.Start( "RKT_Timings" )
					
					//////////////////////////////////////////////////////////////////////////////////////
					//	Execute buildReport in the unique instance                                      //
					//////////////////////////////////////////////////////////////////////////////////////
					buildInstance._buildReport( request, response, DSinstance )
					
					$RKTEngine.Timer.Stop( "RKT_Timings" )
					
					$RKTEngine.Timer.setLogInfo( 'Timestamp', Date.Now() )
					$RKTEngine.Timer.setLogInfo( '_WR Name', request.node.pName )
					$RKTEngine.Timer.setLogInfo( '_WR DataID', request.node.pID )
					$RKTEngine.Timer.setLogInfo( '_TotalSourceRows', BuildInstance.fDSObject.fTotalSourceRows )
					
					logFile = Str.Format( "%1 (%2).csv", request.node.pName, request.node.pID )
					
					$RKTEngine.Timer.DumpToFile( logFile, { 'Timestamp', 'RKT_Timings', '_WR Name', '_WR DataID', '_TotalSourceRows' } )		
				else
					//////////////////////////////////////////////////////////////////////////////////////
					//	Execute buildReport in the unique instance                                      //
					//////////////////////////////////////////////////////////////////////////////////////
					buildInstance._buildReport( request, response, DSinstance )
				end
					
				//////////////////////////////////////////////////////////////////////////////////////
				//	Set initial audit data (further audits occur following export)                  //
				//////////////////////////////////////////////////////////////////////////////////////
				wrWebNode._FirstAuditEvent( prgCtx, request, response, buildInstance, WRnode )
				
				if ( response.error.message )
					return
				end
			
				////////////////////////////////////////////////////////////////////////////////////
			    //   If required, calculate timings and append to the output                      //
			    ////////////////////////////////////////////////////////////////////////////////////
				wrWebNode._insertTimings( response, WRnode, timings )
			
				if ( response.error.message )
					return
				end
			
				////////////////////////////////////////////////////////////////////////////////////
			    //   Deliver the finished report to the specified destination & set the display   //
			    ////////////////////////////////////////////////////////////////////////////////////
				frame3 = $Webreports.WebReportsDestinations._initDestinationObject( prgCtx, request, response, buildInstance, WRnode, APICall )
			
				if ( response.error.message )
					return
				end	
			
				EDinstance = frame3.fTempObject
				EDinstance._runDestinationAll( request, response, buildInstance, WRnode )
			
				// write any destination errors to the WR error log:
				if ( ( BuildInstance.fTagProcessor.fShowErrors ) && ( APIcall ) && ( response.error.message ) )
					response.data.fReturnErrorLog = true
				end
				
				if ( response.error.message )
					return
				end
			
				////////////////////////////////////////////////////////////////////////////////////
				//	Clean up temporary instances											  	  //
				////////////////////////////////////////////////////////////////////////////////////
				//The use of a frame to create any instances means they should automatically cleanup
	
			return response
	
		end

end
