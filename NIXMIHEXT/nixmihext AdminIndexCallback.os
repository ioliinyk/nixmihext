package NIXMIHEXT

public object #'nixmihext AdminIndexCallback'# inherits WEBADMIN::AdminIndexCallback

	override	Boolean	fEnabled = TRUE


	
	override function Assoc Execute( Object prgCtx, Record request )
		
				Assoc		retVal			
				Assoc		sections		
				Dynamic		apiError		
				String		errMsg			
		
				Boolean		ok				 = TRUE
				String		scriptName		 = request.SCRIPT_NAME
				Assoc		section_1		 = ._NewSection()
				Assoc		section_1_1		 = ._NewSection()
		
		
				//main section
				section_1.anchorName = 'NixMihExt'
				section_1.heading = [NIXMIHEXT_LABEL.NixMihExtAdministration]
				sections.( section_1.heading ) = section_1
		
				//subsections
				section_1_1.anchorHREF = Str.Format( "%1?func=nixmihext.ConfigureSAPSystems", scriptName )
				section_1_1.heading = [NIXMIHEXT_LABEL.ConfigureSapSystems]
				section_1_1.text = [NIXMIHEXT_LABEL.AddModifyOrDeleteSapSystems]
				section_1.sections.( section_1_1.heading ) = section_1_1
		
				if ( IsUndefined( retVal.ok ) )
		
					retVal.ok = ok
					retVal.apiError = apiError
					retVal.errMsg = errMsg
					retVal.sections = sections
		
				end
		
				return retVal
		
			end

end
