package NIXMIHEXT

public object Utils inherits NIXMIHEXT::NixmihextRoot

	
	public function Assoc GenerateSapShortcut(
				Object		prgCtx,
				String		sapSystemID,
				String		userSapLogin,
				String		docNumber,
				String		docType,
				String		lang = 'RU' )
		
				Assoc		retVal	
				Assoc		sapSystemData
				Dynamic		checkVal
				String		errMsg	
				String		genShortcutCmd
				String		sapClient
				String		sapName	
				String		sapShortcutContent
		
				Boolean		ok		 = TRUE
		
		
				checkVal = .GetSapSystemData( prgCtx, sapSystemID )
		
				if ( !checkVal.ok )
		
					ok = FALSE
					errMsg = checkVal.errMsg
		
				else
		
					sapSystemData = checkVal.sapSystem.( sapSystemID )
					sapName = sapSystemData.name
					sapclient = sapSystemData.client
					genShortcutCmd = sapSystemData.command
		
					sapShortcutContent = "[System]" + Str.CRLF +
										   "Name=" + sapName + Str.CRLF +
										   "Client=" + sapClient +Str.CRLF +
										   "[User]" +
										   Str.CRLF +
										   "Name=" + userSapLogin + Str.CRLF +
										   "Language=" + lang + Str.CRLF +
										   "[Function]" + Str.CRLF +
										   "Command=" + Str.Format( genShortcutCmd, docNumber, docType) + Str.CRLF +
										   "Type=SystemCommand" + Str.CRLF +
										   "[Configuration]" + Str.CRLF +
										   "GuiSize=Normal window" + Str.CRLF +
										   "[Options]" + Str.CRLF +
										   "Reuse=1" + Str.CRLF
		
				end
		
				retVal.ok = ok
				retVal.errMsg = errMsg
		
				if ( ok )
		
					retval.sapShortcutContent = sapShortcutContent
		
				end
		
				return retVal
		
			end

	
	public function Assoc GetOTDSTicket( Object prgCtx )
		
				Assoc		retval	
				Dynamic		checkVal
				String		errMsg	
				String		ticket	
		
				Boolean		ok		 = TRUE
		
		
				checkVal = OTDSINTEGRATION::Utils.GetResourceTicket( prgCtx )
		
				if ( !checkVal.ok )
		
					ok = FALSE
					errMsg = checkVal.errMsg
		
				else
		
					ticket = checkVal.ticket
		
				end
		
				retval.ok = ok
				retval.errMsg = errMsg
		
				if ( ok )
		
					retval.ticket = ticket
		
				end
		
				return retVal
		
			end

	
	public function Assoc GetSapSystemData( Object prgCtx, String sapSystemID )
		
				Assoc		retVal			
				Assoc		sapSystem		
				Assoc		sapSystemData	
				String		errMsg			
		
				Boolean		ok				 = TRUE
				Assoc		sapSystems		 = prgCtx.IniGet( 'nixmihext', 'sapSystems' )
				Assoc		shortcutGens	 = prgCtx.IniGet( 'nixmihext', 'shortcutGens' )
		
		
				if ( IsFeature( sapSystems, sapSystemID ) && IsFeature( shortcutGens, sapSystemID ) )
		
					sapSystemData = sapSystems.( sapSystemID )
					sapSystemData.command = shortcutGens.( sapSystemID ).command
					sapSystem.( sapSystemID ) = sapSystemData
		
				else
		
					ok = FALSE
					errMsg = Str.format( [NIXMIHEXT_ERRMSG.SapSystemIsNotConfigured], sapSystemID )
		
				end
		
				retval.ok = ok
				retval.errMsg = errMsg
		
				if ( ok )
		
					retval.sapSystem = sapSystem
		
				end
		
				return retVal
		
			end

	
	public function Assoc GetUserSapLogin( Object prgCtx, String userName )
		
				Assoc			checkVal			
				Assoc			responseContent		
				Assoc			retval		
				Dynamic			item				
				List			attrList			
				RestClient		otdsRestClient		
				String			errMsg				
				String			sapLoginAttrValue	
				String			ticket				
				String			userSapLogin		
		
				Assoc			headers				 = Assoc.CreateAssoc()
				Boolean			ok					 = TRUE
				Integer			loopCount = 0
				String			getUserUri			 = "/otdsws/v1/users/"
				String			sapLoginAttrName	 = "oTExternalID2"
		
	
			if ( ok )
	
				while ( loopCount <= 3 )
	
					checkVal = NIXMIHEXT::Utils.GetOTDSTicket( prgCtx )
	
					if ( !checkVal.ok )
	
						ok = FALSE
						errMsg = checkVal.errMsg
	
						break
	
					else
						
						ticket = checkVal.ticket
						
						checkVal = OTDSINTEGRATION::Utils.GetRestClient( prgCtx, getUserUri + userName )
	
						if ( !checkVal.ok )
	
							ok = FALSE
							errMsg = checkVal.errMsg
	
							break
	
						else
	
							otdsRestClient = checkVal.client
	
						end
	
						headers.OTDSTICKET = ticket
	
						checkVal = otdsRestClient.Get( Assoc.CreateAssoc(), headers )
	
						if ( IsFeature( checkVal, 'error' ) )
	
							if ( checkVal.error == 401 )
	
								$OTDSIntegration.Utils.fOTDSTicket = Undefined
	
							else
	
								ok = FALSE
								errMsg = checkVal.message
								break
	
							end
	
						else
	
							break
	
						end
	
					end
	
					loopCount += 1
	
				end
	
			end
	
			if ( ok )
	
				responseContent = Web.FromJSON( checkVal.content )
	
				if ( IsFeature( responseContent, "values" ) )
	
					attrList = responseContent.values
	
					for item in attrList
	
						if ( item.name == sapLoginAttrName )
	
							sapLoginAttrValue = item.values[ 1 ]
	
							break
	
						end
	
					end
	
					if ( IsDefined( sapLoginAttrValue ) )
	
						userSapLogin = Str.Elements( sapLoginAttrValue, '@' )[ 1 ]
	
					end
	
				end
	
			end
	
			retval.ok = ok
			retval.errMsg = errMsg
	
			if ( ok )
	
				retval.userSapLogin = userSapLogin
	
			end
	
			return retVal
	
			end

end
