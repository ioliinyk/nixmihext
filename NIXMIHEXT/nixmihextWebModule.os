package NIXMIHEXT

public object nixmihextWebModule inherits WEBDSP::WebModule

	override	List	fDependencies = { { 'kernel', 16, 2 }, { 'otdsintegration', 16, 2 } }
	override	Boolean	fEnabled = TRUE
	override	String	fModuleName = 'nixmihext'
	override	String	fName = 'N-iX Metinvest Extensions'
	override	List	fOSpaces = { 'nixmihext' }
	override	String	fSetUpQueryString = 'func=nixmihext.configure&module=nixmihext&nextUrl=%1'
	override	List	fVersion = { '16', '2', 'r', '0' }

end
