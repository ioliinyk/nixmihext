package NIXMIHEXT::RH::ADMIN

public object ConfigureSapSystems inherits NIXMIHEXT::RH::ADMIN::#'NixMihExt AdminLLRequestHandler'#

	override	Boolean	fEnabled = TRUE
	override	String	fHTMLFile = 'configuresapsystems.html'
	public		Assoc	fSapSystems
	public		Assoc	fShortcutGens


	/*
				* This is the method subclasses implement to provide their functionality.
				*
				* @param {Dynamic} ctxIn
				* @param {Dynamic} ctxOut
				* @param {Record} request
				*
				* @return {Dynamic} Undefined
				*/
	override function Dynamic Execute( Dynamic ctxIn, Dynamic ctxOut, Record request )
		
				Object		prgCtx			 = .PrgSession()
				Assoc		sapSystems		 = prgCtx.IniGet( 'nixmihext', 'sapSystems' )
				Assoc		shortcutGens	 = prgCtx.IniGet( 'nixmihext', 'shortcutGens' )
		
		
				.fSapSystems = sapSystems
				.fShortcutGens = shortcutGens
		
				return Undefined
		
			end

end
