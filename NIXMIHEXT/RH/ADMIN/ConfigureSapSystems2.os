package NIXMIHEXT::RH::ADMIN

public object ConfigureSapSystems2 inherits NIXMIHEXT::RH::ADMIN::#'NixMihExt AdminLLRequestHandler'#

	override	Boolean	fEnabled = TRUE
	override	String	fHTMLFile = 'configuresapsystems.html'
	public		Assoc	fSapSystems
	public		List	fSapSystemsOrder
	override	Boolean	fSecureRequestTokenEnabled = TRUE
	public		Assoc	fShortcutGens


	/*
				* This is the method subclasses implement to provide their functionality.
				*
				* @param {Dynamic} ctxIn
				* @param {Dynamic} ctxOut
				* @param {Record} request
				*
				* @return {Dynamic} Undefined
				*/
	override function Dynamic Execute( Dynamic ctxIn, Dynamic ctxOut, Record request )
		
				Assoc		sapSystems	
				Assoc		shortcutGens
				Integer		iCount		
				Integer		iRowCount	
				Integer		iRowNum		
				List		sapSystemsOrder
				String		sapSystemID	
				Object		prgCtx		 = .PrgSession()
		
		
				// Get the number of rows on the form.
				iRowCount = Str.StringToInteger( request.RowCount )
		
				if ( request.action == 'addrow' )
		
					// Loop through the form rows
		
					for iCount = 1 to iRowCount
		
						sapSystemID = ._AddSapSystem( iCount, request, sapSystems )
						sapSystemsOrder = { @sapSystemsOrder, sapSystemID }
		
						if ( sapSystemID != '' )
		
							._AddShortcutGen( iCount, request, shortcutGens )
		
						end
		
					end
		
					// Add a blank row
					sapSystemsOrder = { @sapSystemsOrder, '' }
		
					// Return the data to the form
					.fSapSystems = sapSystems
					.fShortcutGens = shortcutGens
					.fSapSystemsOrder = sapSystemsOrder
		
				elseif ( request.action == 'deleterow' )
		
					// Build a List to return to the form.
					iRowNum = Str.StringToInteger( request.rowNum )
		
					// Loop through the form rows
		
					for iCount = 1 to iRowCount
		
						if ( iRowNum != iCount )
		
							sapSystemID = ._AddSapSystem( iCount, request, sapSystems )
							sapSystemsOrder = { @sapSystemsOrder, sapSystemID }
		
							if ( sapSystemID != '' )
		
								._AddShortcutGen( iCount, request, shortcutGens )
		
							end
		
						end
		
					end
		
					// Return the data to the form
					.fSapSystems = sapSystems
					.fShortcutGens = shortcutGens
					.fSapSystemsOrder = sapSystemsOrder
		
				else
		
					// Loop through the form rows
		
					for iCount = 1 to iRowCount
		
						._AddSapSystem( iCount, request, sapSystems )
						._AddShortcutGen( iCount, request, shortcutGens )
		
					end
		
					.fSapSystems = sapSystems
					.fShortcutGens = shortcutGens
		
					// Add the preference into KIni.
		
					if ( Length( Assoc.Keys( sapSystems ) ) > 0 && Length( Assoc.Keys( shortcutGens ) ) > 0 )
		
						prgCtx.IniPut( 'nixmihext', 'sapSystems', sapSystems )
						prgCtx.IniPut( 'nixmihext', 'shortcutGens', shortcutGens )
		
					else
		
						prgCtx.IniDelete( 'nixmihext', 'sapSystems' )
						prgCtx.IniDelete( 'nixmihext', 'shortcutGens' )
		
					end
		
					.fLocation = Str.Format( '%1?func=admin.index', request.SCRIPT_NAME )
		
				end
		
		
		
				return Undefined
		
			end

	
	private function String _AddSapSystem( Integer iCount, Record r, Assoc sapSystems )
		
				Assoc		sapSystemData
				String		ID	
				String		sCount
		
		
				sCount = Str.String( iCount )
				ID = r.( 'row_' + sCount + '_ID' )
		
				if ( ID != '' )
		
					sapSystemData.name = r.( 'row_' + sCount + '_Name' )
					sapSystemData.client = r.( 'row_' + sCount + '_Client' )
					sapSystems.( ID ) = sapSystemData
		
				end
		
				return ID
		
			end

	
	private function void _AddShortcutGen( Integer iCount, Record r, Assoc shortcutGens )
		
				Assoc		shortcutGenData
				String		ID	
				String		sCount
		
		
				sCount = Str.String( iCount )
				ID = r.( 'row_' + sCount + '_ID' )
				shortcutGenData.command = r.( 'row_' + sCount + '_Command' )
				shortcutGens.( ID ) = shortcutGenData
		
			end

end
