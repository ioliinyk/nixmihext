package NIXMIHEXT::RH

public object GetSapShortcut inherits NIXMIHEXT::RH::#'NixMihExt LLRequestHandler'#

	override	Boolean	fEnabled = TRUE
	override	List	fPrototype = { { 'sapSystem', -1, 'SAP System', FALSE }, { 'docNumber', -1, 'SAP document number', FALSE }, { 'docType', -1, 'SAP document type', FALSE } }


	/*
				* This is the method subclasses implement to provide their functionality.
				*
				* @param {Dynamic} ctxIn
				* @param {Dynamic} ctxOut
				* @param {Record} request
				*
				* @return {Dynamic} Undefined
				*/
	override function Dynamic Execute( Dynamic ctxIn, Dynamic ctxOut, Record request )
		
				Assoc		checkVal
				String 		attachFileName
				String		docNumber
				String		docType	
				String		header
				String		sapShortcutContent
				String		sapSystem
				String		userSapLogin
			
				Boolean		ok			 = TRUE
				Object		prgCtx		 = .PrgSession()
				String		userName	 = prgCtx.USession().fUserName
		
		
				sapSystem = request.sapSystem
				docNumber = request.docNumber
				docType = request.docType
				
				if ( Length( sapSystem ) == 0 || Length( docNumber ) == 0 || Length( docType ) == 0 )
		
					ok = FALSE
		
				end
		
				if ( ok )
		
					checkVal = NIXMIHEXT::Utils.GetUserSapLogin( prgCtx, userName )
		
					if ( !checkVal.ok || IsUndefined( checkVal.userSapLogin ) )
		
						ok = FALSE
		
					else
		
						userSapLogin = checkVal.userSapLogin
		
					end
		
				end
		
				if ( ok )
				
					checkVal = NIXMIHEXT::Utils.GenerateSapShortcut(prgCtx, sapSystem, userSapLogin, docNumber, docType )
					
					if ( !checkVal.ok )
		
						ok = FALSE
		
					else
		
						sapShortcutContent = checkVal.sapShortcutContent
		
					end
				
				end
				
				if ( !ok )
		
					.fError = [NIXMIHEXT_ERRMSG.FailedToGenerateSapFileShortcut]
		
				else
		
					//send SAP file shortcut to socket
					attachFileName = sapSystem + '_' + docNumber + '.sap'
					header = Str.Format( "Content-Type: application/octet-stream%1", Web.CRLF )
					header += Str.Format( "Content-Disposition: attachment; filename=%1", attachFileName ) + Web.CRLF
					header += Str.Format( "Content-Length: %1", Str.ByteLength( sapShortcutContent ) ) + Web.CRLF + Web.CRLF
					Web.Write( ctxOut, header )
					Web.Write( ctxOut, sapShortcutContent )
		
					.fHTMLFile = Undefined
		
					if ( Type( ctxOut ) == Socket.SocketType )
		
						Socket.Close( ctxOut )
						.fSocketClosed = TRUE
		
					end
		
				end
		
				return Undefined
		
			end

end
