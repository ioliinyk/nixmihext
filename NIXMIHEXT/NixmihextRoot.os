package NIXMIHEXT

/**
 * 
 *  This is a good place to put documentation about your OSpace.
 *  
 *  
 *  
 *  
 */
public object NixmihextRoot

	public		Object	Globals = NIXMIHEXT::NixmihextGlobals



	/**
					 *  Content Server Startup Code
					 */
	public function Void Startup()
					
						//
						// Initialize globals object
						//
					
						Object	globals = $Nixmihext = .Globals.Initialize()
					
						//
						// Initialize objects with __Init methods
						//
					
						$Kernel.OSpaceUtils.InitObjects( globals.f__InitObjs )
					
					end

end
